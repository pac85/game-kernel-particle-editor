use cgmath;
use game_kernel;
use game_kernel_ecs;

use cgmath::*;
use egui;
use game_kernel::game_kernel::ecs::{AddEntity, EntitId, SystemKey, World};
use game_kernel::game_kernel::subsystems::video::ui::egui::GkEgui;
use game_kernel::game_kernel::subsystems::Subsystems;
use game_kernel::renderer::{
    light::*, material::Material, mesh::*, model::Model, particle_fx::*, texture::*,
};
use game_kernel::game_kernel::MainRenderer;
use imgui::*;

use std::cell::RefCell;
use std::convert::TryFrom;
use std::fs::File;
use std::io::*;
use std::sync::Arc;

mod atlas_editor;
mod particle_editor;

pub struct EguiTexture {
    id: egui::TextureId,
    size: [u32; 2],
}

//this should really be part of the engine perhaps
fn init_systems(sbsts: &mut Subsystems) -> (SystemKey, SystemKey, SystemKey, SystemKey, SystemKey) {
    let mut renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

    let renderer_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::renderer::RendererSystem::new(renderer.clone()),
    ));
    let static_mesh_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::static_mesh::StaticMeshSystem::new(),
    ));
    let light_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::light::LightSystem::new(),
    ));
    let camera_system = Arc::new(RefCell::new(game_kernel::CameraSystem::new()));

    let pfx_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::particle_system::PfxSystem::new(),
    ));

    let rs = sbsts
        .systems_manager
        .add_system(renderer_system)
        .expect("rs");
    let sk = sbsts
        .systems_manager
        .add_system(static_mesh_system)
        .expect("sk");
    let lk = sbsts.systems_manager.add_system(light_system).expect("lk");
    let ck = sbsts.systems_manager.add_system(camera_system).expect("ck");
    let pfxk = sbsts.systems_manager.add_system(pfx_system).expect("pfxk");

    sbsts
        .systems_manager
        .init_system(&rs, sbsts.world.borrow().deref())
        .unwrap();
    sbsts
        .systems_manager
        .init_system(&pfxk, sbsts.world.borrow().deref())
        .unwrap();

    (rs, sk, lk, ck, pfxk)
}

use game_kernel::input::Input;
use std::ops::Deref;

fn setup_input(sbsts: &mut Subsystems) {
    use game_kernel::input::KeyTypes;
    sbsts.input.bind_action(KeyTypes::KeyBoard(17), "forward");
    sbsts.input.bind_action(KeyTypes::KeyBoard(31), "backward");
    sbsts.input.bind_control("forward", "forward", 1.0);
    sbsts.input.bind_control("backward", "forward", -1.0);

    sbsts.input.bind_action(KeyTypes::KeyBoard(32), "left");
    sbsts.input.bind_action(KeyTypes::KeyBoard(30), "right");
    sbsts.input.bind_control("left", "side", 1.0);
    sbsts.input.bind_control("right", "side", -1.0);
}

fn setup_flycam(sbsts: &mut game_kernel::game_kernel::subsystems::Subsystems) -> EntitId {
    //sets up camera
    let camera = game_kernel::Camera::new(
        Point3::new(0.0, 0.0, -6.0),
        Vector3::new(1.0, 0.0, 0.0),
        Vector3::new(0.0, -1.0, 0.0),
        Rad::turn_div_4() / 1f32,
        (0.1, 1000.0),
    );

    let camera_entity = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(game_kernel::CameraComponent::new(camera))
        .entity;
    let camera_id = sbsts
        .systems_manager
        .get_system_by_type::<game_kernel::CameraSystem>()
        .unwrap();
    sbsts
        .systems_manager
        .get_system(camera_id)
        .unwrap()
        .borrow_mut()
        .downcast_mut::<game_kernel::CameraSystem>()
        .unwrap()
        .set_active_camera(camera_entity);

    camera_entity
}

fn flycam<W: Deref<Target = World>>(
    world: W,
    camera_entity_index: EntitId,
    delta_time: f32,
    r: Vector2<f32>,
    input: &Input,
) {
    let components_mutex = world.get_entity_components(camera_entity_index).unwrap();
    let components = components_mutex;
    let mut camerac = components.iter().next().unwrap().borrow_mut();
    let camera = camerac
        .gk_as_any_mut()
        .downcast_mut::<game_kernel::CameraComponent>()
        .unwrap();

    camera.camera.dir =
        cgmath::Vector3::new(r.x.sin() * r.y.cos(), -r.y.sin(), r.x.cos() * r.y.cos());

    use cgmath::prelude::InnerSpace;
    let velocity = if input.is_action_down("sprint") {
        0.04
    } else {
        0.02
    };

    camera.camera.eye +=
        camera.camera.dir * delta_time * velocity * input.get_control_value("forward").unwrap();
    camera.camera.eye += camera.camera.dir.cross(camera.camera.up).normalize()
        * delta_time
        * velocity
        * input.get_control_value(&"side".to_owned()).unwrap();
}

#[derive(PartialEq)]
pub enum Tab {
    ParticleEditor,
    AtlasEditor,
}

impl Tab {
    pub fn name(&self) -> &'static str {
        match self {
            Self::ParticleEditor => "✨ particle editor",
            Self::AtlasEditor => "🖼 atlas editor",
        }
    }
}

struct UIState {
    selected_tab: Tab,
    atlas_state: atlas_editor::UIState,
    particle_state: particle_editor::UIState,
}

impl UIState {
    pub fn new(renderer: Arc<RefCell<MainRenderer>>, default_atlas: Texture) -> Self {
        Self {
            selected_tab: Tab::ParticleEditor,
            atlas_state: atlas_editor::UIState::new(),
            particle_state: particle_editor::UIState::new(renderer, default_atlas),
        }
    }
}

pub fn manage_tab(seleceted: &mut Tab, tab: Tab, ui: &mut egui::Ui) {
    if ui.selectable_label(*seleceted == tab, tab.name()).clicked() {
        *seleceted = tab;
    }
}

fn render_ui(
    gk_egui: &mut GkEgui,
    state: &mut UIState,
    delta: f32,
) -> bool {
    let ctx = &gk_egui.platform.context();
    let mut should_run = true;
    //main menu bar
    egui::TopBottomPanel::top("top bar").show(ctx, |ui| {
        egui::menu::bar(ui, |ui| {
            egui::menu::menu(ui, "file", |ui| {
                should_run = !ui.button("exit").clicked();
            });
            ui.separator();
            ui.add_space(30.0);
            manage_tab(&mut state.selected_tab, Tab::ParticleEditor, ui);
            manage_tab(&mut state.selected_tab, Tab::AtlasEditor, ui);
        });
    });

    match state.selected_tab {
        Tab::ParticleEditor => state.particle_state.render_ui(gk_egui),
        Tab::AtlasEditor => state.atlas_state.render_ui(gk_egui, delta),
    }

    should_run
}

fn main() {
    let mut sbsts = game_kernel::game_kernel::subsystems::init().unwrap();

    let (render_system_key, _, light_system_key, camera_system_key, pfx_key) =
        init_systems(&mut sbsts);

    let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

    let camera_entity = setup_flycam(&mut sbsts);

    setup_input(&mut sbsts);

    let mut camera_rotation = Vector2::new(0.0f32, 0.0f32);

    let treader1 = BufReader::new(File::open("fire.ktx").unwrap());
    let atlas_texture_data =
        game_kernel::game_kernel::subsystems::video::renderer::texture::TextureData::load_from_kxt(
            treader1,
        )
        .unwrap();
    //particle system
    let test_atlas = {
        game_kernel::game_kernel::subsystems::video::renderer::texture::Texture::from_texture_data(
            &atlas_texture_data,
            game_kernel::game_kernel::subsystems::video::renderer::texture::TextureUsage::Color,
            sbsts.get_vulkan_common().unwrap(),
            Some(SRGB),
        )
        .unwrap()
    };

    let renderer_cell = sbsts.renderer.renderer.as_ref().unwrap().clone();
    {
        let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();
        let cylinder_emitter = CylinderEmitter::new(
            Vector3::new(0.0, 0.0, 0.0),
            0.05,
            0.05,
            Vector3::new(0.0, 0.0, 0.0),
            Vector3::new(0.3, 0.3, 0.3),
            38.1,
            Matrix3::identity(),
        );

        /*let pfx_key = renderer
            .borrow_mut()
            .build_particle_system(
                &GenericParticleSystemBuilder::<_, _, AlphaParticleRenderer>::new(
                    Box::new(cylinder_emitter),
                    AlphaParticleRendererParams {
                        life: 1.1,
                        size: Vector2::new(0.5, 0.5),
                        texture: ParticleTexture {
                            atlas: test_atlas.clone(),
                            offset: Vector2::new(0.0, 0.0),
                            size: Vector2::new(1.0 / 16.0, 1.0 / 5.0),
                            fps: Vector2::new(30.0, 30.0 / 16.0),
                            frames: Vector2::new(16, 5),
                        },
                        blending: ParticleBlending::Additive,
                    },
                    |vk, max| {
                        Box::new(SimpleParticlePhysics::new(
                            vk,
                            max,
                            Vector3::new(0.0, 1.0, 0.0),
                        ))
                    },
                    50,
                ),
            );*/
    }
    //particle system

    let test_texture = include_bytes!("../test.data");
    let test_texture = test_texture
        .chunks_exact(3)
        .flat_map(|p| [p[0], p[1], p[2], 255u8])
        .collect::<Vec<_>>();

    let mut test_texture_id = None;
    //main loop
    let mut ui_state = UIState::new(renderer_cell.clone(), test_atlas.clone());
    sbsts.main_loop(move |delta, mouse_pos, buttons, world, ui, input, gkegui| {
        camera_rotation += mouse_pos / 1000f32;
        camera_rotation.y = camera_rotation.y.min(3.1415 / 2.0).max(-3.1415 / 2.0);

        flycam(world, camera_entity, delta, camera_rotation, input);

        std::thread::sleep_ms(14);

        /*egui::Window::new("lol")
        .resizable(true)*/

        if test_texture_id.is_none() {
            //test_texture_id = Some(gkegui.painter.alloc_user_texture(test_texture.iter().cloned(), [31, 25], true).unwrap());
            test_texture_id = Some(
                gkegui
                    .painter
                    .alloc_user_texture(
                        atlas_texture_data.layers_data[0]
                            .iter()
                            .collect::<Box<[_]>>()
                            .chunks_exact(3)
                            .flat_map(|p| [*p[0], *p[1], *p[2], 255u8])
                            .collect::<Vec<_>>()
                            .iter()
                            .cloned(),
                        atlas_texture_data.size,
                        true,
                    )
                    .unwrap(),
            );

            ui_state.atlas_state.atlas_texture = Some(EguiTexture {
                id: test_texture_id.unwrap(),
                size: atlas_texture_data.size,
            });
        }
        let eguictx = &gkegui.platform.context();
        /*egui::SidePanel::right("lol").show(eguictx, |ui| {
            ui.label("lol");
            ui.button("Hi");
            ui.separator();
            let scroll = egui::ScrollArea::from_max_height(200.0);
            scroll.show(ui, |ui| {
                ui.vertical(|ui| {
                    for i in 1..100 {
                        ui.label(format!("Hi {}", i));
                    }
                });
            });
            ui.separator();
            ui.horizontal(|ui| {
                for _ in 1..5 {
                    ui.button("test");
                }

                ui.separator();

                for _ in 1..5 {
                    ui.button("test");
                }
            });

            let s = atlas_texture_data.size;
            let response = ui.image(
                test_texture_id.unwrap(),
                egui::vec2(s[0] as f32 / 5 as f32, s[1] as f32 / 5 as f32),
            );
            let painter = ui.painter();
            let visuals = ui.style().noninteractive();
            if let Some(p) = response.hover_pos() {
                painter.circle(p, 20.0, egui::Color32::YELLOW, visuals.fg_stroke);
                painter.line_segment(
                    [
                        egui::pos2(p.x, response.rect.top()),
                        egui::pos2(p.x, response.rect.bottom()),
                    ],
                    visuals.fg_stroke,
                );
                painter.line_segment(
                    [
                        egui::pos2(response.rect.left(), p.y),
                        egui::pos2(response.rect.right(), p.y),
                    ],
                    visuals.fg_stroke,
                );
            }
        });*/

        render_ui(gkegui, &mut ui_state, delta)
    });

    println!("Bye");
}
