use super::GkEgui;

use egui;
use cgmath::*;
use game_kernel::renderer::{
    particle_fx::*, texture::*,
};
use game_kernel::game_kernel::MainRenderer;

//TODO import from engine
type KeyType = u64;

use std::cell::RefCell;
use std::sync::Arc;

pub struct ParticleEffect {
    name: String,
    key: KeyType,
}

impl ParticleEffect {
    pub fn name(&self) -> String {
        self.name.clone()
    }
}

pub struct UIState {
    particle_effects: Vec<ParticleEffect>,
    selected_effect: Option<usize>,
    renderer: Arc<RefCell<MainRenderer>>,
    default_atlas: Texture,
    new_name: String,
    dialogs: Vec<Box<dyn Fn(&egui::CtxRef) -> bool>>,
}

impl UIState {
    pub fn new(renderer: Arc<RefCell<MainRenderer>>, default_atlas: Texture) -> Self {
        Self {
            particle_effects: vec![],
            selected_effect: None,
            renderer,
            default_atlas,
            new_name: "".to_owned(),
            dialogs: vec![],
        }
    }

    fn add_particle_effect(&mut self) {
        if self.particle_effects.iter().find(|s| s.name() == self.new_name).is_some() {
            let name = self.new_name.clone();
            self.dialogs.push(Box::new(move |ctx| {
                let mut should_close = false;
                egui::Window::new("Cant add particle system").show(ctx, |ui| {
                    ui.label(format!("there's already a particle system called {}", name));
                    should_close = ui.button("ok").clicked(); 
                });
                should_close
            }));
            return;
        }
        let cylinder_emitter = CylinderEmitter::new(
            Vector3::new(0.0, 0.0, 0.0),
            0.05,
            0.05,
            Vector3::new(0.0, 0.0, 0.0),
            Vector3::new(0.3, 0.3, 0.3),
            38.1,
            Matrix3::identity(),
        );
        let key = self.renderer
            .borrow_mut()
            .build_particle_system(
                &GenericParticleSystemBuilder::<_, _, AlphaParticleRenderer>::new(
                    Box::new(cylinder_emitter),
                    AlphaParticleRendererParams {
                        life: 1.1,
                        size: Vector2::new(0.5, 0.5),
                        texture: ParticleTexture {
                            atlas: self.default_atlas.clone(),
                            offset: Vector2::new(0.0, 0.0),
                            size: Vector2::new(1.0 / 16.0, 1.0 / 5.0),
                            fps: Vector2::new(30.0, 30.0 / 16.0),
                            frames: Vector2::new(16, 5),
                        },
                        blending: ParticleBlending::Additive,
                    },
                    |vk, max| {
                        Box::new(SimpleParticlePhysics::new(
                            vk,
                            max,
                            Vector3::new(0.0, 1.0, 0.0),
                        ))
                    },
                    50,
                ),
            );

        self.particle_effects.push(ParticleEffect {
            name: self.new_name.clone(),
            key,
        });
    }

    fn remove_particle_effect(&mut self, i: usize) {
        let key = self.particle_effects.get(i);
        if key.is_none() {
            return;
        }
        let key = key.unwrap().key;
        self.renderer.borrow_mut().remove_particle_system(key);
        self.particle_effects.remove(i);
    }

    fn manage_dialogs(&mut self, ctx: &egui::CtxRef) {
        let mut to_close = vec![];
        to_close.resize(self.dialogs.len(), false);
        for (i, dialog) in self.dialogs.iter().enumerate() {
            to_close[i] = (dialog)(ctx);
        }

        for (i, should_close) in to_close.iter().enumerate().rev() {
            if *should_close {
                self.dialogs.remove(i);
            }
        }
    }

    fn draw_sidebar(&mut self, gk_egui: &mut GkEgui) {
        let ctx = &gk_egui.platform.context();

        egui::SidePanel::right("lol").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.add(
                    egui::widgets::TextEdit::singleline(&mut self.new_name)
                        .hint_text("name")
                        .desired_width(80.0),
                );
                if ui.button("+").clicked() {
                    self.add_particle_effect();
                }
                if ui.button("-").clicked() && self.selected_effect.is_some() {
                    self.remove_particle_effect(self.selected_effect.unwrap());
                    self.selected_effect = None;
                }
            });
            egui::ScrollArea::from_max_height(200.0).show(ui, |ui| {
                ui.vertical(|ui| {
                    for (i, particle_effect) in self.particle_effects.iter().enumerate() {
                        if ui
                            .selectable_label(
                                self.selected_effect.is_some()
                                    && self.selected_effect.unwrap() == i,
                                particle_effect.name(),
                            )
                            .clicked()
                        {
                            self.selected_effect = Some(i);
                        }
                    }
                });
            });
        });
    }

    fn get_pfx_key(&self) -> Option<KeyType> {
        Some(self.particle_effects[self.selected_effect?].key)
    }

    pub fn draw_editor_window(&mut self, gk_egui: &mut GkEgui) {
        let ctx = &gk_egui.platform.context();
        let key = self.get_pfx_key();
        if key.is_none() {
            return;
        }
        let key = key.unwrap();
        let mut psg = self.renderer.borrow_mut();
        let ps = psg.get_particle_system_mut(key).unwrap(); 

        egui::Window::new("todo").show(ctx, |ui| {
            ui.separator();
            ui.label("physics");
            ui.separator();
            {
                if let Some(physics) = ps.get_particle_physics_mut().gk_as_any_mut().downcast_mut::<SimpleParticlePhysics>() {
                    ui.label("accelleration");
                    let mut accel = physics.get_accelleration();
                    ui.add(egui::widgets::Slider::new(
                        &mut accel.x,
                        -20.0..=20.0,
                    ));
                    ui.add(egui::widgets::Slider::new(
                        &mut accel.y,
                        -20.0..=20.0,
                    ));
                    ui.add(egui::widgets::Slider::new(
                        &mut accel.z,
                        -20.0..=20.0,
                    ));
                    physics.update_accelleration(accel);
                }
            }     

            ui.separator();
            ui.label("emitter");
            ui.separator();
            ui.horizontal(|ui| {

                let emitter = ps.get_emitter_mut().gk_as_any_mut().downcast_mut::<CylinderEmitter>().unwrap();
                ui.label("emission rate");
                ui.add(egui::widgets::Slider::new(
                    &mut emitter.emission_rate,
                    0.0..=240.0,
                ));
            });
        });
    }

    pub fn render_ui(
        &mut self,
        gk_egui: &mut GkEgui,
    ) {
        self.draw_editor_window(gk_egui);
        let ctx = &gk_egui.platform.context();
        self.manage_dialogs(ctx);
        self.draw_sidebar(gk_egui);
    }
}
