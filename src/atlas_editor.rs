use egui;
use game_kernel::game_kernel::subsystems::video::renderer::texture::TextureData;
use game_kernel::game_kernel::subsystems::video::ui::egui::GkEgui;
use std::fs::File;
use std::io::BufReader;
use std::sync::mpsc::{channel, Receiver};

pub const ATLAS_UI_SIZE: f32 = 600.0;
pub const PREVIEW_UI_SIZE: f32 = 100.0;
pub const TEXTURES_DIR: &'static str = "textures/";

#[derive(PartialEq)]
pub enum Tool {
    FrameSize,
    AtlasRegion,
    None,
}

pub enum AtlasLoadState {
    NotLoaded,
    Loaded,
    Loading(Receiver<TextureData>),
}

impl Default for AtlasLoadState {
    fn default() -> Self {
        Self::Loaded
    }
}

#[derive(Clone)]
pub struct ParticleTextureInfo {
    path: String,
    framerate: f32,
    n_frames: [u32; 2],
    region: [u32; 4],
}

pub struct UIState {
    texture_files: Vec<String>,
    files_receiver: Option<Receiver<Vec<String>>>,
    selected_idx: Option<usize>,
    atlas_loading: AtlasLoadState,

    pub atlas_texture: Option<super::EguiTexture>,
    pub tool: Tool,
    pub ui_atlas_size: egui::Vec2,
    pub selected_frame: egui::Rect,
    pub elapsed: f32,
    pub current_frame: u32,
    pub current_pt: ParticleTextureInfo,
}

impl UIState {
    pub fn new() -> Self {
        Self {
            texture_files: vec![],
            files_receiver: None,
            selected_idx: None,
            atlas_loading: AtlasLoadState::Loaded,

            atlas_texture: None,
            tool: Tool::None,
            ui_atlas_size: egui::vec2(0.0, 0.0),
            selected_frame: egui::Rect::from_min_max(egui::pos2(0.0, 0.0), egui::pos2(0.0, 0.0)),
            //n_frames: [1; 2],
            elapsed: 0.0,
            //framerate: 0.0,
            current_frame: 0,
            //selected_region: [0, 0, 1, 1],
            current_pt: ParticleTextureInfo {
                path: "".to_owned(),
                framerate: 0.0,
                n_frames: [1; 2],
                region: [0, 0, 1, 1],
            },
        }
    }

    fn reset_region(&mut self) {
        self.current_pt.region = [0; 4];
        self.current_pt.region[2] = self.current_pt.n_frames[0];
        self.current_pt.region[3] = self.current_pt.n_frames[1];
    }

    fn draw_tool(&mut self, ui: &mut egui::Ui, response: &egui::Response) {
        let painter = ui.painter();

        if let Some(hover_pos) = response.hover_pos() {
            if response.is_pointer_button_down_on() {
                self.selected_frame = egui::Rect::from_min_max(self.selected_frame.min, hover_pos);
            }
        }

        match self.tool {
            Tool::FrameSize => {
                if let Some(hover_pos) = response.hover_pos() {
                    if !response.is_pointer_button_down_on() {
                        if self.selected_frame.top() != self.selected_frame.bottom()
                            && self.selected_frame.left() != self.selected_frame.right()
                        {
                            let n_frames = [
                                (response.rect.size().x / self.selected_frame.size().x) as u32,
                                (response.rect.size().y / self.selected_frame.size().y) as u32,
                            ];
                            if n_frames.iter().all(|x| *x < 100 && *x > 0) {
                                self.current_pt.n_frames = n_frames;
                                self.reset_region();
                            }
                            self.tool = Tool::None;
                        }
                        self.selected_frame = egui::Rect::from_min_max(hover_pos, hover_pos);
                    }
                }
                painter.rect_stroke(
                    response.rect.intersect(self.selected_frame),
                    2.0,
                    egui::Stroke::new(1.0, egui::Color32::LIGHT_BLUE),
                );
            }
            Tool::AtlasRegion => {
                if let Some(hover_pos) = response.hover_pos() {
                    if !response.is_pointer_button_down_on() {
                        if self.selected_frame.top() != self.selected_frame.bottom()
                            && self.selected_frame.left() != self.selected_frame.right()
                        {
                            let (left, top, right, bottom) = self.selected_frame_rect(response);
                            let size = response.rect.size();
                            self.current_pt.region[0] =
                                (left / size.x * self.current_pt.n_frames[0] as f32) as u32;
                            self.current_pt.region[1] =
                                (top / size.y as f32 * self.current_pt.n_frames[1] as f32) as u32;
                            self.current_pt.region[2] =
                                (right / size.x * self.current_pt.n_frames[0] as f32).ceil() as u32;
                            self.current_pt.region[3] =
                                (bottom / size.y * self.current_pt.n_frames[1] as f32).ceil()
                                    as u32;

                            //make sure at least one frame is selected
                            if self.current_pt.region[0] == self.current_pt.region[2] {
                                self.current_pt.region[2] += 1;
                            }
                            if self.current_pt.region[1] == self.current_pt.region[3] {
                                self.current_pt.region[3] += 1;
                            }

                            self.tool = Tool::None;
                        }
                        self.selected_frame = egui::Rect::from_min_max(hover_pos, hover_pos);
                    }
                }
                painter.rect_stroke(
                    response.rect.intersect(self.selected_frame),
                    2.0,
                    egui::Stroke::new(1.0, egui::Color32::GREEN),
                );
            }
            Tool::None => {}
        }
    }

    fn selected_frame_rect(&self, response: &egui::Response) -> (f32, f32, f32, f32) {
        (
            self.selected_frame.left() - response.rect.left(),
            self.selected_frame.top() - response.rect.top(),
            self.selected_frame.right() - response.rect.left(),
            self.selected_frame.bottom() - response.rect.top(),
        )
    }

    fn selected_region_absolute_rect(&self, response: &egui::Response) -> egui::Rect {
        let size = response.rect.size();
        let start_uv = egui::vec2(
            self.current_pt.region[0] as f32 / self.current_pt.n_frames[0] as f32,
            self.current_pt.region[1] as f32 / self.current_pt.n_frames[1] as f32,
        );
        let end_uv = egui::vec2(
            self.current_pt.region[2] as f32 / self.current_pt.n_frames[0] as f32,
            self.current_pt.region[3] as f32 / self.current_pt.n_frames[1] as f32,
        );
        egui::Rect {
            min: response.rect.min + size * start_uv,
            max: response.rect.min + size * end_uv,
        }
    }

    fn get_frame_n(&self, i: u32, j: u32) -> Option<u32> {
        if i < self.current_pt.region[0]
            || j < self.current_pt.region[1]
            || i >= self.current_pt.region[2]
            || j >= self.current_pt.region[3]
        {
            return None;
        }
        let selected_frames_n = self.current_pt.region[2] - self.current_pt.region[0];
        Some((i - self.current_pt.region[0]) + (j - self.current_pt.region[1]) * selected_frames_n)
    }

    fn get_frame_coords(&self, frame: u32) -> (u32, u32) {
        let n_frames = self.selected_n_frames();
        (
            frame % n_frames[0] + self.current_pt.region[0],
            (frame / n_frames[0]) % n_frames[1] + self.current_pt.region[1],
        )
    }

    fn get_frame(&self) -> u32 {
        let n_frames = self.selected_n_frames();
        self.current_frame % (n_frames[0] * n_frames[1])
    }

    fn selected_n_frames(&self) -> [u32; 2] {
        [
            self.current_pt.region[2] - self.current_pt.region[0],
            self.current_pt.region[3] - self.current_pt.region[1],
        ]
    }

    fn get_frame_uv_rect(&self, frame: u32) -> egui::Rect {
        let (i, j) = self.get_frame_coords(frame);
        let frame_left_top = egui::vec2(i as f32, j as f32)
            / egui::vec2(
                self.current_pt.n_frames[0] as f32,
                self.current_pt.n_frames[1] as f32,
            );
        let frame_size = egui::vec2(
            1 as f32 / self.current_pt.n_frames[0] as f32,
            1 as f32 / self.current_pt.n_frames[1] as f32,
        );
        egui::Rect::from_min_size(egui::pos2(frame_left_top.x, frame_left_top.y), frame_size)
    }

    fn draw_grid(&mut self, ui: &mut egui::Ui, response: &egui::Response) {
        let old_clip = ui.clip_rect();
        ui.set_clip_rect(self.selected_region_absolute_rect(response).expand(1.0));
        let painter = ui.painter();
        //grid lines
        for i in 0..=self.current_pt.n_frames[0] {
            let x = i as f32 / self.current_pt.n_frames[0] as f32;
            let width = response.rect.size().x;
            let absx = response.rect.left() + width * x;
            painter.line_segment(
                [
                    egui::pos2(absx, response.rect.top()),
                    egui::pos2(absx, response.rect.bottom()),
                ],
                egui::Stroke::new(1.0, egui::Color32::GRAY),
            );
        }

        for j in 0..=self.current_pt.n_frames[1] {
            let y = j as f32 / self.current_pt.n_frames[1] as f32;
            let height = response.rect.size().y;
            let absy = response.rect.top() + height * y;
            painter.line_segment(
                [
                    egui::pos2(response.rect.left(), absy),
                    egui::pos2(response.rect.right(), absy),
                ],
                egui::Stroke::new(1.0, egui::Color32::GRAY),
            );
        }

        //frame numbers
        for i in 0..self.current_pt.n_frames[0] {
            for j in 0..self.current_pt.n_frames[1] {
                let frame_left_top = response.rect.left_top()
                    + egui::vec2(i as f32, j as f32)
                        / egui::vec2(
                            self.current_pt.n_frames[0] as f32,
                            self.current_pt.n_frames[1] as f32,
                        )
                        * response.rect.size();
                //draw text if inside the selected region
                if let Some(frame_n) = self.get_frame_n(i, j) {
                    painter.text(
                        egui::pos2(5.0, 5.0) + frame_left_top.to_vec2(),
                        egui::Align2::LEFT_TOP,
                        frame_n,
                        egui::TextStyle::Small,
                        if self.get_frame() != frame_n {
                            egui::Color32::GRAY
                        } else {
                            egui::Color32::GREEN
                        },
                    );
                }
            }
        }
        //restore clip rect
        ui.set_clip_rect(old_clip);
    }

    fn draw_atlas(&mut self, ui: &mut egui::Ui) {
        if let Some(texture) = &self.atlas_texture {
            let max_dim = texture.size[0].max(texture.size[1]) as f32;
            let factor = ATLAS_UI_SIZE / max_dim;
            let response = ui.add(
                egui::Image::new(
                    texture.id,
                    egui::vec2(
                        texture.size[0] as f32 * factor,
                        texture.size[1] as f32 * factor,
                    ),
                )
                .sense(egui::Sense::click_and_drag()),
            );
            self.ui_atlas_size = response.rect.size();

            if self.tool != Tool::FrameSize {
                self.draw_grid(ui, &response);
            }
            self.draw_tool(ui, &response);

            let visuals = ui.style().noninteractive();
            let painter = ui.painter();
            if let Some(hover_pos) = response.hover_pos() {
                painter.line_segment(
                    [
                        egui::pos2(hover_pos.x, response.rect.top()),
                        egui::pos2(hover_pos.x, response.rect.bottom()),
                    ],
                    visuals.fg_stroke,
                );
                painter.line_segment(
                    [
                        egui::pos2(response.rect.left(), hover_pos.y),
                        egui::pos2(response.rect.right(), hover_pos.y),
                    ],
                    visuals.fg_stroke,
                );
            }
        }
    }

    fn draw_preview(&self, ui: &mut egui::Ui) {
        let size = self.ui_atlas_size
            / egui::vec2(
                self.current_pt.n_frames[0] as f32,
                self.current_pt.n_frames[1] as f32,
            );
        let max_dim = size.x.max(size.y) as f32;
        let factor = PREVIEW_UI_SIZE / max_dim;

        if let Some(texture) = &self.atlas_texture {
            ui.add(
                egui::Image::new(texture.id, size * factor)
                    .uv(self.get_frame_uv_rect(self.current_frame)),
            );
        }
    }

    fn start_atlas_update(&mut self, gk_egui: &mut GkEgui) {
        let (send, receive) = channel();
        self.atlas_loading = AtlasLoadState::Loading(receive);
        let old = self.atlas_texture.take();
        if let Some(old) = old {
            gk_egui.painter.free_user_texture(old.id);
        }

        if let Some(si) = self.selected_idx {
            let filename = TEXTURES_DIR.to_owned() + &self.texture_files[si];
            std::thread::spawn(move || {
                let reader = BufReader::new(File::open(filename).unwrap());

                let atlas_data = TextureData::load_from_kxt(reader).unwrap();
                send.send(atlas_data)
                    .unwrap_or_else(|e| println!("{:?}", e));
            });
        }
    }

    fn manage_atlas_update(&mut self, gk_egui: &mut GkEgui) {
        if let AtlasLoadState::Loading(receiver) = &self.atlas_loading {
            if let Ok(atlas_data) = receiver.try_recv() {
                let has_alpha = atlas_data.gl_internal_format == 35907;
                let id = gk_egui
                    .painter
                    .alloc_user_texture(
                        atlas_data.layers_data[0]
                            .iter()
                            .collect::<Box<[_]>>()
                            .chunks_exact(if has_alpha { 4 } else { 3 })
                            .flat_map(|p| [*p[0], *p[1], *p[2], 255u8])
                            .collect::<Vec<_>>()
                            .iter()
                            .cloned(),
                        atlas_data.size,
                        true,
                    )
                    .unwrap();
                self.atlas_texture = Some(super::EguiTexture {
                    id,
                    size: atlas_data.size,
                });
                self.atlas_loading = AtlasLoadState::Loaded;
            }
        }
    }

    fn draw_side_panel(&mut self, gk_egui: &mut GkEgui) {
        let ctx = &gk_egui.platform.context();
        egui::SidePanel::right("lol").show(ctx, |ui| {
            ui.label("Select texture");
            ui.separator();
            egui::ScrollArea::from_max_height(200.0).show(ui, |ui| {
                ui.vertical(|ui| {
                    for (i, texture_file) in self.texture_files.iter().enumerate() {
                        if ui
                            .selectable_label(
                                self.selected_idx.is_some() && self.selected_idx.unwrap() == i,
                                format!("{}", texture_file),
                            )
                            .clicked()
                        {
                            if let Some(selected) = self.selected_idx {
                                if selected != i {
                                    self.atlas_loading = AtlasLoadState::NotLoaded;
                                }
                            }
                            self.selected_idx = Some(i);
                        }
                    }
                });
            });
            ui.separator();
        });
    }

    pub fn render_ui(&mut self, gk_egui: &mut GkEgui, delta: f32) {
        match self.atlas_loading {
            AtlasLoadState::NotLoaded => {
                self.start_atlas_update(gk_egui);
            }
            _ => {}
        }
        self.manage_atlas_update(gk_egui);
        let ctx = &gk_egui.platform.context();
        if self.elapsed == 0.0 {
            let (send, receiver) = channel();
            self.files_receiver = Some(receiver);

            std::thread::spawn(move || {
                let texts = std::fs::read_dir(std::path::Path::new(TEXTURES_DIR))
                    .unwrap()
                    //.filter(|p| p.map(|p| p.path().file_name().map(|f| f.e)))
                    .map(|p| p.unwrap().file_name().to_str().unwrap().to_string())
                    .collect::<Vec<_>>();
                send.send(texts).unwrap();
            });
        }

        self.elapsed += delta;
        self.current_frame = (self.current_pt.framerate * self.elapsed / 1000.0) as u32;

        if let Some(Ok(files)) = self.files_receiver.as_mut().map(|r| r.try_recv()) {
            self.texture_files = files;
        }

        egui::Window::new("Atlas Editor").show(ctx, |ui| {
            ui.horizontal(|ui| {
                if ui
                    .selectable_label(self.tool == Tool::FrameSize, "select frame")
                    .clicked()
                {
                    self.tool = Tool::FrameSize;
                }
                if ui
                    .selectable_label(self.tool == Tool::AtlasRegion, "atlas region")
                    .clicked()
                {
                    self.tool = Tool::AtlasRegion;
                }
            });
            self.draw_atlas(ui);
            ui.separator();
            ui.horizontal(|ui| {
                self.draw_preview(ui);
                ui.vertical(|ui| {
                    ui.add(egui::widgets::Slider::new(
                        &mut self.current_pt.framerate,
                        0.0..=240.0,
                    ));
                    let mut s_n_frames_x = self.current_pt.n_frames[0].to_string();
                    let mut s_n_frames_y = self.current_pt.n_frames[1].to_string();
                    ui.horizontal(|ui| {
                        ui.add(
                            egui::widgets::TextEdit::singleline(&mut s_n_frames_x)
                                .desired_width(0.0),
                        );
                        ui.add(
                            egui::widgets::TextEdit::singleline(&mut s_n_frames_y)
                                .desired_width(0.0),
                        );
                    });
                    if let Ok(x) = s_n_frames_x.parse() {
                        self.current_pt.n_frames[0] = x;
                    } else if s_n_frames_x.is_empty() {
                        self.current_pt.n_frames[0] = 1;
                    }
                    if let Ok(y) = s_n_frames_y.parse() {
                        self.current_pt.n_frames[1] = y;
                    } else if s_n_frames_y.is_empty() {
                        self.current_pt.n_frames[1] = 1;
                    }
                })
            });
            if ui.button("save").clicked() {}
        });

        self.draw_side_panel(gk_egui);
    }
}
